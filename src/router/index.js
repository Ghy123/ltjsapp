import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login'
import Yzheng from '@/components/Yzheng'
import xilieChoose from '@/components/xilieChoose'
import AboutMac from '@/components/AboutMac'
import WriterMac from '@/components/WriterMac'
import MacInfoQueren from '@/components/MacInfoQueren'
import MyMac from '@/components/MyMac'
import MacInfo from '@/components/MacInfo'
import MacZhenduan from '@/components/MacZhenduan'
import MacBaoyang from '@/components/MacBaoyang'
import Proplem from '@/components/Proplem'
import MacBaoyangInfo from '@/components/MacBaoyangInfo'
import WithoutMac from'@/components/WithoutMac'

import Dingdan from '@/components/Dingdan/Dingdan'
import DingdanRoom from '@/components/Dingdan/DingdanRoom'
import All from '@/components/Dingdan/All'
import Tiaosi from '@/components/Dingdan/Tiaosi'
import Weixiu from '@/components/Dingdan/Weixiu'
import Gaizao from '@/components/Dingdan/Gaizao'
import Other from '@/components/Dingdan/Other'
import DingdanInfo from '@/components/Dingdan/DingdanInfo'
import DingdanPingjia from '@/components/Dingdan/DingdanPingjia'
import LingpeijianDingdan from '@/components/Dingdan/LingpeijianDingdan'
import LingpeijianDingdanInfo from '@/components/Dingdan/LingpeijianDingdanInfo'
import SearchOrders from '@/components/Dingdan/SearchOrders'
import MineAll from '@/components/Dingdan/MineAll'
import WriteSummary from '@/components/Dingdan/WriteSummary'
import SumSuccess from '@/components/Dingdan/SumSuccess'
import DingdanMacInfo from '@/components/Dingdan/DingdanMacInfo'
import standard from '@/components/Dingdan/standard'
import customer from '@/components/Dingdan/customer'

import Lingpeijian from '@/components/Order/Lingpeijian'
import Chooseplace from '@/components/Order/Chooseplace'
import NewPlace from '@/components/Order/NewPlace'
import ChooseLingpeijian from '@/components/Order/ChooseLingpeijian'
import MacChange from '@/components/Order/MacChange'

import InfAudit from '@/components/InformationsAudit/InfAudit'
import NotAudit from '@/components/InformationsAudit/NotAudit'
import Audited from '@/components/InformationsAudit/Audited'
import InfAuditInfos from '@/components/InformationsAudit/InfAuditInfos'
import NotPassReason from '@/components/InformationsAudit/NotPassReason'
import AuditedPass from '@/components/InformationsAudit/AuditedPass' 

import Xiaoxi from '@/components/News/Xiaoxi'
import XiTongNews from '@/components/News/XiTongNews'
import DingdanXiaoxi from '@/components/News/DingdanXiaoxi'
import IMChat from '@/components/News/IMChat'

import My from '@/components/Mine/My'
// import MyLingpeijian from '@/components/Mine/MyLingpeijian'
// import MyAboutMac from '@/components/Mine/MyAboutMac'
// import MyAboutMacInfo from '@/components/Mine/MyAboutMacInfo'
// import MyPinglun from '@/components/Mine/MyPinglun'
// import MyPlace from '@/components/Mine/MyPlace'
// import MyPlaceChange from '@/components/Mine/MyPlaceChange'
import MyDingDan from '@/components/Mine/MyDingDan'
import MyInformation from '@/components/Mine/MyInformation'
import MyPhone from '@/components/Mine/MyPhone'
import MyPhoneNext from '@/components/Mine/MyPhoneNext'
import MyWorkResult from '@/components/Mine/MyWorkResult'
import AboutLongTeng from '@/components/Mine/AboutLongTeng'
import MyProblem from '@/components/Mine/MyProblem'
import MySet from '@/components/Mine/MySet'
import BanbenInfo from '@/components/Mine/BanbenInfo'
import ChangeName from '@/components/Mine/ChangeName'
import MyInformation from '@/components/Mine/MyInformation'
import MyPhone from '@/components/Mine/MyPhone'
import MyPhoneNext from '@/components/Mine/MyPhoneNext'
import MyWorkResult from '@/components/Mine/MyWorkResult'
import MyOrder from '@/components/Mine/MyOrder'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: login
    },{
      path: '/Yzheng',
      name: 'Yzheng',
      component: Yzheng
    },{
      path: '/xilieChoose',
      name: 'xilieChoose',
      component: xilieChoose
    },{
      path: '/AboutMac/:id',
      name: 'AboutMac',
      component: AboutMac
    },{
      path: '/WriterMac/:id',
      name: 'WriterMac',
      component: WriterMac
    },{
      path: '/MacInfoQueren/:id',
      name: 'MacInfoQueren',
      component: MacInfoQueren
    },{
      // path: '/MyMac/:id',
      path: '/MyMac/',
      name: 'MyMac',
      component: MyMac,
      children:[
        {
          path:'/MyMac/MacInfo/',
          name: 'MacInfo',
          component: MacInfo
        },
        {
          path:'/MyMac/DingdanRoom/All',
          name: 'DingdanRoom',
          component: DingdanRoom,
          children:[
            {
              path:'/MyMac/DingdanRoom/All',
              name: 'All',
              component: All
            },
            {
              path:'/MyMac/DingdanRoom/Tiaosi',
              name: 'Tiaosi',
              component: Tiaosi
            },
            {
              path:'/MyMac/DingdanRoom/Weixiu',
              name: 'Weixiu',
              component: Weixiu
            },
            {
              path:'/MyMac/DingdanRoom/Gaizao',
              name: 'Gaizao',
              component: Gaizao
            },
            {
              path:'/MyMac/DingdanRoom/Other',
              name: 'Other',
              component: Other
            },
            {
              path:'/MyMac/DingdanRoom/LingpeijianDingdan',
              name: 'LingpeijianDingdan',
              component: LingpeijianDingdan
            },
          ]
        },
        {
          path:'/MyMac/Dingdan/',
          name: 'Dingdan',
          component: Dingdan,
          children:[
            {
              path:'/MyMac/Dingdan/MineAll',
              name: 'MineAll',
              component: MineAll
            },
            // {
            //   path:'/MyMac/Dingdan/Tiaosi',
            //   name: 'Tiaosi',
            //   component: Tiaosi
            // },
            // {
            //   path:'/MyMac/Dingdan/Weixiu',
            //   name: 'Weixiu',
            //   component: Weixiu
            // },
            // {
            //   path:'/MyMac/Dingdan/Gaizao',
            //   name: 'Gaizao',
            //   component: Gaizao
            // },
            // {
            //   path:'/MyMac/Dingdan/Other',
            //   name: 'Other',
            //   component: Other
            // },
            // {
            //   path:'/MyMac/Dingdan/LingpeijianDingdan',
            //   name: 'LingpeijianDingdan',
            //   component: LingpeijianDingdan
            // },
          ]
        },
        {
          path:'/MyMac/My',
          name: 'My',
          component: My
        },
        {
          path:'/MyMac/Xiaoxi',
          name: 'Xiaoxi',
          component: Xiaoxi
        },{
          path: '/MyMac/InfAudi/NotAudit',
          name: 'InfAudit',
          component: InfAudit,
          children:[
            {
              path:'/MyMac/InfAudi/NotAudit',
              name:'NotAudit',
              component: NotAudit
            },
            {
              path:'/MyMac/InfAudi/Audited',
              name:'Audited',
              component: Audited
            }
          ]
        }
      ]
    },{
      path: '/MacZhenduan',
      name: 'MacZhenduan',
      component: MacZhenduan
    },{
      path: '/MacBaoyang',
      name: 'MacBaoyang',
      component: MacBaoyang
    },{
      path: '/MacBaoyangInfo/:id',
      name: 'MacBaoyangInfo',
      component: MacBaoyangInfo
    },{
      path: '/Proplem/:id',
      name: 'Proplem',
      component: Proplem
    },{
      path: '/DingdanInfo/:id',
      name: 'DingdanInfo',
      component: DingdanInfo
    },{
      path: '/WriteSummary/:id',
      name: 'WriteSummary',
      component: WriteSummary
    },{
      path: '/LingpeijianDingdanInfo/:id',
      name: 'LingpeijianDingdanInfo',
      component: LingpeijianDingdanInfo
    },{
      path: '/DingdanPingjia/:id',
      name: 'DingdanPingjia',
      component: DingdanPingjia
    },{
      path: '/MacChange/',
      name: 'MacChange',
      component: MacChange
    },{
      path: '/Lingpeijian/',
      name: 'Lingpeijian',
      component: Lingpeijian
    },{
      path: '/Chooseplace/',
      name: 'Chooseplace',
      component: Chooseplace
    },{
      path: '/NewPlace/',
      name: 'NewPlace',
      component: NewPlace
    },{
      path: '/MyLingpeijian/',
      name: 'MyLingpeijian',
      component: MyLingpeijian
    },{
      path: '/SumSuccess/:id',
      name: 'SumSuccess',
      component: SumSuccess
    },{
      path: '/MyAboutMac/',
      name: 'MyAboutMac',
      component: MyAboutMac
    },{
      path: '/MyInformation/',
      name: 'MyInformation',
      component: MyInformation
    },{
      path: '/MyPhone/',
      name: 'MyPhone',
      component: MyPhone
    },{
      path: '/MyPhoneNext/',
      name: 'MyPhoneNext',
      component: MyPhoneNext
    },{
      path: '/MyWorkResult/',
      name: 'MyWorkResult',
      component: MyWorkResult
    },{
      path: '/MyOrder/',
      name: 'MyOrder',
      component: MyOrder
    },{
      path: '/MyAboutMacInfo/:id',
      name: 'MyAboutMacInfo',
      component: MyAboutMacInfo
    },{
      path: '/MyPinglun/',
      name: 'MyPinglun',
      component: MyPinglun
    },{
      path: '/MyPlace/',
      name: 'MyPlace',
      component: MyPlace
    },{
      path: '/MyPlaceChange/:id',
      name: 'MyPlaceChange',
      component: MyPlaceChange
    },{
      path: '/AboutLongTeng/',
      name: 'AboutLongTeng',
      component: AboutLongTeng
    },{
      path: '/MyProblem/',
      name: 'MyProblem',
      component: MyProblem
    },{
      path: '/MySet/',
      name: 'MySet',
      component: MySet
    },{
      path: '/BanbenInfo/',
      name: 'BanbenInfo',
      component: BanbenInfo
    },{
      path: '/ChangeName/',
      name: 'ChangeName',
      component: ChangeName
    },{
      path: '/XiTongNews/',
      name: 'XiTongNews',
      component: XiTongNews
    },{
      path: '/DingdanXiaoxi/',
      name: 'DingdanXiaoxi',
      component: DingdanXiaoxi
    },{
      path: '/IMChat/:id',
      name: 'IMChat',
      component: IMChat
    },{
      path: '/ChooseLingpeijian/',
      name: 'ChooseLingpeijian',
      component: ChooseLingpeijian
    },{
      path:'/WithoutMac/',
      name: 'WithoutMac',
      component: WithoutMac
    },{
      path:'/SearchOrders/',
      name: 'SearchOrders',
      component: SearchOrders
    },{
      path:'/InfAuditInfos/:id',
      name: 'InfAuditInfos',
      component: InfAuditInfos
    },{
      path:'/NotPassReason/:id',
      name: 'NotPassReason',
      component: NotPassReason
    },{
      path:'/AuditedPass/:id',
      name: 'AuditedPass',
      component: AuditedPass
    },{
      path:'/DingdanMacInfo/standard',
      name: 'DingdanMacInfo',
      component: DingdanMacInfo,
      children:[
        {
          path:'/DingdanMacInfo/standard',
          name: 'standard',
          component: standard
        },{
          path:'/DingdanMacInfo/customer',
          name: 'customer',
          component: customer
        }
      ]
    }
  ],
})
// 需要左方向动画的路由用this.$router.togo('****')
Router.prototype.togo = function (path) {
  this.isleft = true
  this.isright = false
  this.push(path)
}
// 需要右方向动画的路由用this.$router.goRight('****')
Router.prototype.goRight = function (path) {
  this.isright = true
  this.isleft = false
  this.push(path)
}
// 需要返回按钮动画的路由用this.$router.goBack()，返回上一个路由
Router.prototype.goBack = function () {
  this.isright = true
  this.isleft = false
  this.go(-1)
}
// 点击浏览器返回按钮执行，此时不需要路由回退
Router.prototype.togoback = function () {
  this.isright = true
  this.isleft = false
}